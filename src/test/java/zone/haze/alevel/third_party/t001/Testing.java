package zone.haze.alevel.third_party.t001;

import org.junit.Assert;
import org.junit.Test;

import static zone.haze.alevel.third_party.t001.Task.sum;

public class Testing {

    @Test
    public void testSum1() {
        int expected = 45;

        int[] test = new int[]{1, 2, 33, 19, 3, 9, 17};
        int result = sum(test, 3);

        assertionHelper(expected, result);
    }

    @Test
    public void testSum2() {
        int expected = 20;

        int[] test = new int[]{2, 3, 8, 19, 3, 9, 17, 6, 11, 7, 4};
        int result = sum(test, 2);

        assertionHelper(expected, result);
    }

    @Test
    public void testSum3() {
        int expected = 196;

        int[] test = new int[]{45, 2, 49, 19, 70, 9, 77, 900, 12458, 89, 36752, 48};
        int result = sum(test, 7);

        assertionHelper(expected, result);
    }

    @Test
    public void testSum4() {
        int expected = 221;

        int[] test = new int[]{1, 2, 52, 33, 19, 3, 9, 17, 130, 13, 26};
        int result = sum(test, 13);

        assertionHelper(expected, result);
    }

    @Test
    public void testSum5() {
        int expected = 117;

        int[] test = new int[]{1, 2, 18, 33, 19, 3, 9, 17, 90, 47};
        int result = sum(test, 9);

        assertionHelper(expected, result);
    }

    private void assertionHelper(int expected, int result) {
        Assert.assertEquals(String.format(
                "Ожидание %d. Реальность %d.", expected, result
        ), expected, result);
    }

}

package zone.haze.alevel.third_party.t004;

import org.junit.Assert;
import org.junit.Test;

public class Testing {

    @Test
    public void test01() {
        int result = Task.riseOrFall();

        Assert.assertEquals("А должен быть нуль.", 0, result);
    }

    @Test
    public void test02() {
        int result = Task.riseOrFall(1, 2, 3);

        Assert.assertEquals("Это явная восходящая последовательность.", 1, result);
    }

    @Test
    public void test03() {
        int result = Task.riseOrFall(3, 2, 1);

        Assert.assertEquals("Это явная нисходящая последовательность.", -1, result);
    }

    @Test
    public void test04() {
        int result = Task.riseOrFall(1, 2, 1, 2, 1);

        Assert.assertEquals("А должен быть нуль. Неявная прямая последовательность.", 0, result);
    }

    @Test
    public void test05() {
        int result = Task.riseOrFall(10, 9, 1, 3, 6, 2, 4, 5, 11, 9, 10, 11);

        Assert.assertEquals("Неявная восходящая последовательность.", 1, result);
    }

    @Test
    public void test06() {
        int result = Task.riseOrFall(1, 2, 3, 4, 3, 9, 8, 7, 5, 4, 1, 18, 17, 16, 15, 10, 8, 4, 2);

        Assert.assertEquals("Неявная нисходящая последовательность.", -1, result);
    }

}

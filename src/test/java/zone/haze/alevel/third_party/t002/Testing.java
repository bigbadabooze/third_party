package zone.haze.alevel.third_party.t002;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Testing {
    private StringBuilder builder = new StringBuilder();

    private int getRandomInt() {
        return (int) (Math.random() * 50);
    }

    private void check(int[] result, int[] origin) {
        Arrays.stream(result).forEach(integer -> Assert.assertEquals("Не ноль.", 0, origin[integer]));
    }

    private void check(Integer[] result, Integer[] origin) {
        Arrays.stream(result).forEach(integer -> Assert.assertEquals("Не ноль.", Integer.valueOf(0), origin[integer]));
    }

    private void check(List<Integer> result, List<Integer> origin) {
        result.forEach(integer -> Assert.assertEquals("Не ноль.", Integer.valueOf(0), origin.get(integer)));
    }

    private void prettyArrayPrint(int[] array) {
        builder.setLength(0);

        builder.append('[');
        Arrays.stream(array).forEach(integer -> builder.append(integer).append(", "));
        builder.setLength(builder.length() - 2);
        builder.append(']');

        System.out.println(builder.toString());
    }

    private void prettyArrayPrint(Integer[] array) {
        builder.setLength(0);

        builder.append('[');
        Arrays.stream(array).forEach(integer -> builder.append(integer).append(", "));
        builder.setLength(builder.length() - 2);
        builder.append(']');

        System.out.println(builder.toString());
    }

    @Test
    public void test01() {
        int[] origin = IntStream.range(0, 256).map(i -> getRandomInt()).toArray();
        prettyArrayPrint(origin);

        int[] zeros = Task.indexesOfZeros(origin);
        prettyArrayPrint(zeros);

        check(zeros, origin);
    }

    @Test
    public void test02() {
        test01();
    }

    @Test
    public void test03() {
        test01();
    }

    @Test
    public void test04() {
        test01();
    }

    @Test
    public void test05() {
        test01();
    }

    @Test
    public void test06() {
        test01();
    }

    @Test
    public void test07() {
        test01();
    }

    @Test
    public void test08() {
        test01();
    }

    @Test
    public void test09() {
        test01();
    }

    @Test
    public void test10() {
        test01();
    }

    @Test
    public void test11() {
        Integer[] origin = IntStream.range(0, 256).mapToObj(i -> getRandomInt()).toArray(Integer[]::new);
        prettyArrayPrint(origin);

        Integer[] zeros = Task.indexesOfZeros(origin);
        prettyArrayPrint(zeros);

        check(zeros, origin);
    }

    @Test
    public void test12() {
        test11();
    }

    @Test
    public void test13() {
        test11();
    }

    @Test
    public void test14() {
        test11();
    }

    @Test
    public void test15() {
        test11();
    }

    @Test
    public void test16() {
        List<Integer> origin = IntStream.range(0, 256).mapToObj(i -> getRandomInt()).collect(Collectors.toCollection(() -> new ArrayList<>(256)));
        List<Integer> zeros = Task.indexesOfZeros(origin);

        System.out.println(origin);
        System.out.println(zeros);

        check(zeros, origin);
    }

    @Test
    public void test17() {
        test16();
    }

    @Test
    public void test18() {
        test16();
    }

    @Test
    public void test19() {
        test16();
    }

    @Test
    public void test20() {
        test16();
    }

    @Test(expected = AssertionError.class)
    public void test21() {
        List<Integer> origin = IntStream.range(0, 256).mapToObj(i -> getRandomInt()).collect(Collectors.toCollection(() -> new ArrayList<>(256)));
        List<Integer> zeros = Task.indexesOfZeros(origin);

        origin.add(1);
        zeros.add(256);

        System.out.println(origin);
        System.out.println(zeros);

        check(zeros, origin);
    }

}

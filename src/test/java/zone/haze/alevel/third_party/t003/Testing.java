package zone.haze.alevel.third_party.t003;

import org.junit.Test;

public class Testing {

    // ToDo: Ehm... what to check here?

    @Test
    public void test01() {
        Task.positiveOrNegative(new int[]{0, 0, 1, -2, -4, 9, 0});
    }

    @Test
    public void test02() {
        Task.positiveOrNegative(new Integer[]{0, 0, 1, -2, -4, 9, 0});
    }

    @Test
    public void test03() {
        Task.positiveOrNegative(new int[]{0, 0, -2, -4, 9, 0});
    }

    @Test
    public void test04() {
        Task.positiveOrNegative(new int[]{0, 0, 0});
    }

    @Test
    public void test05() {
        Task.positiveOrNegative(new int[0]);
    }

}

package zone.haze.alevel.third_party.t001;

import java.util.Arrays;

/**
 * Дан массив натуральных чисел. Найти сумму элементов, кратных данному K.
 */
public class Task {

    public static int sum(int[] arr, int k) {
        return Arrays.stream(arr).reduce(0, (left, right) -> left + (right % k == 0 ? right : 0));
    }

}

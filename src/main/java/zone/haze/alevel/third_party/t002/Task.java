package zone.haze.alevel.third_party.t002;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * В целочисленной последовательности есть нулевые элементы. Создать массив из номеров этих элементов.
 */
public class Task {

    public static List<Integer> indexesOfZeros(List<Integer> ints) {
        return IntStream.range(0, ints.size()).filter(i -> ints.get(i) == 0).boxed().collect(Collectors.toCollection(ArrayList::new));
    }

    public static Integer[] indexesOfZeros(Integer[] ints) {
        return indexesOfZeros(Arrays.asList(ints)).toArray(new Integer[0]);
    }

    public static int[] indexesOfZeros(int[] ints) {
        List<Integer> resultsCollection = indexesOfZeros(Arrays.stream(ints).boxed().collect(Collectors.toCollection(ArrayList::new)));

        int[] result = new int[resultsCollection.size()];

        Arrays.setAll(result, resultsCollection::get);

        return result;
    }

}

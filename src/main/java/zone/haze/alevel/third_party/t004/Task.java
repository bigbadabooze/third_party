package zone.haze.alevel.third_party.t004;

/**
 * Дана последовательность действительных чисел a 1 , a 2 , ..., a n . Выяснить, будет ли она возрастающей.
 */
public class Task {

    public static int riseOrFall(int... ints) {
        if (ints.length < 2) {
            return 0;
        }

        int rising = 0;
        int falling = 0;
        for (int i = 0; i < ints.length - 1; i++) {
            if (ints[i] < ints[i + 1]) {
                rising++;
            } else if (ints[i] > ints[i + 1]) {
                falling++;
            }
        }

        return Integer.compare(rising, falling);
    }

}

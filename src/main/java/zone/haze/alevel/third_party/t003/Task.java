package zone.haze.alevel.third_party.t003;

/**
 * Дана последовательность целых чисел a 1 , a 2 , ..., a n . Выяснить, какое число встречается раньше –
 * положительное или отрицательное.
 */
public class Task {

    public static void positiveOrNegative(int... ints) {
        if (ints.length < 1) {
            System.out.println("Нет чисел для проверки.");
            return;
        }

        int now = 0;

        for (int anInt : ints) {
            if (anInt > 0) {
                now = 1;
                break;
            } else if (anInt < 0) {
                now = -1;
                break;
            }
        }

        resolveNow(now);
    }

    public static void positiveOrNegative(Integer... ints) {
        if (ints.length < 1) {
            System.out.println("Нет чисел для проверки.");
            return;
        }

        int now = 0;

        for (Integer anInt : ints) {
            if (anInt > 0) {
                now = 1;
                break;
            } else if (anInt < 0) {
                now = -1;
                break;
            }
        }

        resolveNow(now);
    }

    private static void resolveNow(int now) {
        switch (now) {
            case 1:
                System.out.println("Первым встречается положительное число.");
                break;
            case -1:
                System.out.println("Первым встречается отрицательное число.");
                break;
            case 0:
                System.out.println("Последовательность состоит из нулей.");
                break;
            default:
                System.out.println("Что-то пошло не так.");
        }
    }

}
